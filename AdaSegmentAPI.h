/*
AdaBoost classification based on Dr. Hongzhi Wang's AdaBoost code.
*/

template<class ProbabilityImage>
typename ProbabilityImage::Pointer
adaSegment(const char* inputImage, const char* abdominalMaskImage, const char* AdaBoostOutPutPrefix );
